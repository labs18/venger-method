#include <fstream>
#include <vector>

using namespace std;

const int INF = 10e6;

ifstream fin("input.txt");
ofstream fout("output.txt");

int main()
{
	int n, m;
	fin >> n >> m;
	vector<vector<int>> a(n + 1, vector<int>(m + 1));

	for (size_t i = 1; i < n + 1; i++)
		for (size_t j = 1; j < m + 1; j++)
			fin >> a[i][j];

	fin.close();


	vector<int> u(n + 1),
				v(m + 1),
				p(m + 1),
				way(m + 1);

	for (int i = 1; i <= n; ++i)
	{
		p[0] = i;
		int j0 = 0;
		vector<int> minv(m + 1, INF);
		vector<bool> used(m + 1, false);

		do
		{
			used[j0] = true;
			int i0 = p[j0];
			int delta = INF;
			int j1;

			for (int j = 1; j <= m; ++j)
				if (!used[j])
				{
					int cur = a[i0][j] - u[i0] - v[j];

					if (cur < minv[j])
					{
						minv[j] = cur;
						way[j] = j0;
					}

					if (minv[j] < delta)
					{
						delta = minv[j];
						j1 = j;
					}
				}

			for (int j = 0; j <= m; ++j)
				if (used[j])
				{
					u[p[j]] += delta;
					v[j] -= delta;
				}
				else
					minv[j] -= delta;

			j0 = j1;
		} while (p[j0]);

		do {
			p[j0] = p[way[j0]];
			j0 = way[j0];
		} while (j0);
	}

	vector<int> ans(n + 1);
	for (int j = 1; j <= m; ++j)
		ans[p[j]] = j;

	fout << -v[0] << endl;
	for (size_t i = 1; i < n + 1; i++)
		fout << ans[i] << ' ';

	fout.close();

	return 0;
}